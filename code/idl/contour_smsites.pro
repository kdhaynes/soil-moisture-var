;;This program plots up a map of soil moisture data.
;;
;;kdhaynes, 2019/04

pro contour_smsites,data,lon,lat,contours=contours,limit=limit, $
    title=title, ctitle=ctitle, psFile=psFile, comment=comment

dsize=size(data)
dsize=dsize(1)

if (n_elements(limit) eq 0) THEN BEGIN
   minlat=40.6643
   maxlat=40.66600
   minlon=-105.0001
   maxlon=-104.9960
   limit=[minlat,minlon,maxlat,maxlon]
ENDIF
lats = [40.6644,40.6648,40.6652,40.6656,40.666]
lons = [-104.9992,-104.9986,-104.9980,-104.9974,-104.9968]

deltai=0.0001
deltaj=0.0001

if (n_elements(psFile) ne 0) then begin
    set_plot,'PS'
    !p.font=0
    myheight=4.
    myratio=1.6
    mywidth=myheight*myratio
    device,filename=psFile,xsize=mywidth,ysize=myheight,/inches, $
        color=8,font_size=6,/encapsulated,bits_per_pixel=8
endif else begin
   set_display
   set_plot_colors,scheme=1
endelse

;.....
; Set up arrays to hold grid coordinates
missing_value=-999.99
latNW = FLTARR(dsize)-missing_value
lonNW = FLTARR(dsize)-missing_value
latNE = FLTARR(dSize)-missing_value
lonNE = FLTARR(dSize)-missing_value
latSE = FLTARR(dSize)-missing_value
lonSE = FLTARR(dSize)-missing_value
latSW = FLTARR(dSize)-missing_value
lonSW = FLTARR(dSize)-missing_value

for i=0L,dsize-1 do begin
   if (lat(i) ne -999.99 and lon(i) ne missing_value) then begin
     latNW(i) = lat(i)+deltaj
     if (latNW(i) gt 90.) then latNW(i)=90.
     lonNW(i) = lon(i)-deltai
     if (lonNW(i) gt 180) then lonNW(i)=180.
     latNE(i) = lat(i)+deltaj
     if (latNE(i) gt 90) then latNE(i)=90. 
     lonNE(i) = lon(i)+deltai
     if (lonNE(i) gt 180) then lonNE(i)=180
     latSW(i) = lat(i)-deltaj
     if (latSW(i) lt -90) then latSW(i)=-90
     lonSW(i) = lon(i)-deltai
     if (lonSW(i) lt -180) then lonSW(i)=-180
     latSE(i) = lat(i)-deltaj
     if (latSE(i) lt -90) then latSE(i)=-90
     lonSE(i) = lon(i)+deltai
     if (lonSE(i) lt -180) then lonSE(i)=-180
    endif
endfor

;Set up contour levels based on max and min of data
if (n_elements(contours) eq 0) then begin
 nlevels=12
 gooddata = where(data ne missing_value)
 gooddata = data(gooddata)
 plotmin = min(gooddata)
 plotmax = max(gooddata)
 interval = (plotmax-plotmin)/(nlevels+1)
 contours = plotmin + findgen(nlevels)*interval
endif else begin
 nlevels=size(contours)
 nlevels=nlevels(1)
endelse

;Set up color table to make plot
nColors=255
colorFactor = nColors/(nlevels+1)

scaleddata = bytarr(dsize)
for level = 0L, nlevels-1 do begin
    points = where(data ge contours[level],npoints)
    if npoints ne 0 then scaleddata[points] = byte((level+1)*colorfactor)
endfor

;Set map coordinates
map_set,/mercator,/isotropic,position=[.1,.2,.9,.88], $
        limit=limit
xyouts, 0.5, 0.95, title, /normal, charsize=2.4, align=0.5
xyouts, 0.5, 0.90, comment, /normal, charsize=1.6, align=0.5

;Set map grid
;latstr = string(lats,format='(F12.4)')
;latstr[4] = ''
;lonstr = string(lons,format='(F12.4)')
;lonstr[4] = ''
;map_grid, lats = lats, latnames=latstr, label=1, latlab=-104.9965, $
;          charsize=1.2, lonlab=lats[0], lons=lons
map_grid

;Fill in grid boxes with color according to value of data
for i=0L,dsize-1 do begin
   if (data[i] ne missing_value) then begin
      if (scaleddata[i] eq 0) then scaleddata[i]=255B
      polyfill,color=scaleddata[i], $
          [lonNW[i],lonNE[i],lonSE[i],lonSW[i]], $
          [latNW[i],latNE[i],latSE[i],latSW[i]]
   endif
endfor

;Add color bar
color_bar,contours,1,labelFormat='(F10.2)',cbar_csize=1.8, $
          cbar_tcolor=0,whiteout=2, cbar_title=ctitle, $
          position=[0.14,0.10,0.84,0.14]
 
;color_bar,contours,1,labelFormat='(I4)'  

if (n_elements(psFile) gt 0) then begin
   device, /close
   set_plot, 'X'
endif

end



