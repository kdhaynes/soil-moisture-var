;;This program maps the soil moisture content per probe.
;;
;;kdhaynes, 2019/04

pro mapdt_smsites

  idepth=4   ;0-4
  itime=12   ;0-12
  plot_idlcontour = 0
  plot_mycontour = 1
  
  read_smdata, sitenum, sitelon, sitelat, smdata, $
               nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
               varnames=varnames
  timenames=varnames(3:ntimes+2)
  dnames=['6','18','30','42','54']
  badval=-900
  
  data = smdata(*,idepth,itime)
  goodref=where(data gt badval)
  data = data(goodref)
  sitelon = sitelon(goodref)
  sitelat = sitelat(goodref)
  
  ctitle = 'Soil Moisture (Volumetric)'
  title = timenames(itime) + ' 2012 At Depth ' $
          + dnames(idepth) + ' Inches'
  
  minlat=40.6644
  maxlat=40.66600
  minlon=-104.9998
  maxlon=-104.9960
  mylimit = [minlat,minlon,maxlat,maxlon]
  nsites=n_elements(sitelon)

  if (plot_mycontour) then begin
     contour_smsites,data,sitelon,sitelat,limit=mylimit, $
               ctitle=ctitle, title=title
  endif

  if (plot_idlcontour) then begin
      close_windows
      gridData=SPH_SCAT(sitelon,sitelat,data, $
                    bounds=[minlon,minlat,maxlon,maxlat], $
                    GS=[0.0001,0.0001], BOUT=bout)
      s=size(gridData)
      xlon = FINDGEN(s(1))*((bout(2)-bout(0))/(s(1)-1)) + bout(0)
      ylat = FINDGEN(s(2))*((bout(3)-bout(1))/(s(2)-1)) + bout(1)

      m = MAP('Robinson',LIMIT=mylimit, $
              margin=[0.06,0.06,0.06,0.06], title=title)

      grid = m.mapgrid
      grid.linestyle="dotted"
      grid.label_position=0
      grid.font_size=0
  
      ct = COLORTABLE(72,/reverse)
      mymin=min(gridData)
      mymax=max(gridData)
      c = CONTOUR(gridData, xlon, ylat, $
              /fill, overplot=m, rgb_table=ct, $
               grid_units='degrees', $
               min_value=mymin, max_value=mymax, $
              TITLE=filename)
      cp = COLORBAR(TITLE=ctitle,rgb_table=ct, $
                    range=[mymin,mymax], $
                    position=[0.2,0.1,0.8,0.14])
   endif
  
end


;;This program plots up a map of soil moisture data.
;;
;;kdhaynes, 2019/04

pro contour_smsites,data,lon,lat,contours=contours,limit=limit, $
    title=title, ctitle=ctitle, psFile=psFile

dsize=size(data)
dsize=dsize(1)

if (n_elements(limit) eq 0) THEN BEGIN
   minlat=40.6644
   maxlat=40.66600
   minlon=-104.9994
   maxlon=-104.9962
   limit=[minlat,minlon,maxlat,maxlon]
ENDIF
deltai=0.0001
deltaj=0.0001

if (n_elements(psFile) ne 0) then begin
    set_plot,'PS'
    !p.font=0
    myheight=4.
    myratio=1.6
    mywidth=myheight*myratio
    device,filename=psFile,xsize=mywidth,ysize=myheight,/inches, $
        color=8,font_size=6,/encapsulated,bits_per_pixel=8
endif else begin
   set_display
   set_plot_colors,scheme=1
endelse

;.....
; Set up arrays to hold grid coordinates
missing_value=-999.99
latNW = FLTARR(dsize)-missing_value
lonNW = FLTARR(dsize)-missing_value
latNE = FLTARR(dSize)-missing_value
lonNE = FLTARR(dSize)-missing_value
latSE = FLTARR(dSize)-missing_value
lonSE = FLTARR(dSize)-missing_value
latSW = FLTARR(dSize)-missing_value
lonSW = FLTARR(dSize)-missing_value

for i=0L,dsize-1 do begin
   if (lat(i) ne -999.99 and lon(i) ne missing_value) then begin
     latNW(i) = lat(i)+deltaj
     if (latNW(i) gt 90.) then latNW(i)=90.
     lonNW(i) = lon(i)-deltai
     if (lonNW(i) gt 180) then lonNW(i)=180.
     latNE(i) = lat(i)+deltaj
     if (latNE(i) gt 90) then latNE(i)=90. 
     lonNE(i) = lon(i)+deltai
     if (lonNE(i) gt 180) then lonNE(i)=180
     latSW(i) = lat(i)-deltaj
     if (latSW(i) lt -90) then latSW(i)=-90
     lonSW(i) = lon(i)-deltai
     if (lonSW(i) lt -180) then lonSW(i)=-180
     latSE(i) = lat(i)-deltaj
     if (latSE(i) lt -90) then latSE(i)=-90
     lonSE(i) = lon(i)+deltai
     if (lonSE(i) lt -180) then lonSE(i)=-180
    endif
endfor

;Set up contour levels based on max and min of data
if (n_elements(contours) eq 0) then begin
 nlevels=12
 gooddata = where(data ne missing_value)
 gooddata = data(gooddata)
 plotmin = min(gooddata)
 plotmax = max(gooddata)
 interval = (plotmax-plotmin)/(nlevels+1)
 contours = plotmin + findgen(nlevels)*interval
endif else begin
 nlevels=size(contours)
 nlevels=nlevels(1)
endelse

;Set up color table to make plot
nColors=255
colorFactor = nColors/(nlevels+1)

scaleddata = bytarr(dsize)
for level = 0L, nlevels-1 do begin
    points = where(data ge contours[level],npoints)
    if npoints ne 0 then scaleddata[points] = byte((level+1)*colorfactor)
endfor

;Set map coordinates
map_set,/mercator,/isotropic,position=[.1,.2,.9,.9], $
       limit=limit, title=title

;Fill in grid boxes with color according to value of data
for i=0L,dsize-1 do begin
   if (data[i] ne missing_value) then begin
      if (scaleddata[i] eq 0) then scaleddata[i]=255B
      polyfill,color=scaleddata[i], $
          [lonNW[i],lonNE[i],lonSE[i],lonSW[i]], $
          [latNW[i],latNE[i],latSE[i],latSW[i]]
   endif
endfor

;Add color bar
color_bar,contours,1,labelFormat='(F10.2)',cbar_csize=1.8, $
    cbar_tcolor=0,whiteout=2, cbar_title=ctitle
 
;color_bar,contours,1,labelFormat='(I4)'  
end



