pro read_smdata, sitenum, sitelon, sitelat, sm, $
                 nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
                 varnames=varnames

  nsites=41
  ndepths=5
  ntimes=13

  filedir1 = '/Users/kdhaynes/Dropbox/'
  filedir2 = 'classes/cs440/project/AllNeutronProbeReadings/'
  filenames=['Vol_At06in.csv','Vol_At18in.csv','Vol_At30in.csv', $
             'Vol_At42in.csv','Vol_At54in.csv']
  
  sm = fltarr(nsites,ndepths,ntimes)
  for d=0,ndepths-1 do begin
     myfilename = filedir1 + filedir2 + filenames[d]
     read_csv_simpro,myfilename,sitenum,sitelon,sitelat, $
           sm_t1, sm_t2, sm_t3, sm_t4, sm_t5, sm_t6, sm_t7, $
           sm_t8, sm_t9, sm_t10, sm_t11, sm_t12, sm_t13, $
           varname=varnames

     sm(*,d,0) = sm_t1(*)
     sm(*,d,1) = sm_t2(*)
     sm(*,d,2) = sm_t3(*)
     sm(*,d,3) = sm_t4(*)
     sm(*,d,4) = sm_t5(*)
     sm(*,d,5) = sm_t6(*)
     sm(*,d,6) = sm_t7(*)
     sm(*,d,7) = sm_t8(*)
     sm(*,d,8) = sm_t9(*)
     sm(*,d,9) = sm_t10(*)
     sm(*,d,10) = sm_t11(*)
     sm(*,d,11) = sm_t12(*)
     sm(*,d,12) = sm_t13(*)
  endfor

end
