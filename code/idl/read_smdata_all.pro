pro read_smdata_all, sitenum, sitelon, sitelat, sm, $
                 nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
                 varnames=varnames

  nsites=41
  ndepths=5
  ntimes=13

  filedir1 = '/Users/kdhaynes/Dropbox/'
  filedir2 = 'classes/cs440/project/AllNeutronProbeReadings/'
  filename='Vol_AllFI.csv'
  
  sm = fltarr(nsites,ndepths,ntimes)
  myfilename = filedir1 + filedir2 + filename
  read_csv_simpro,myfilename,sitenuma,sitelona,sitelata, $
           sm_t1, sm_t2, sm_t3, sm_t4, sm_t5, sm_t6, sm_t7, $
           sm_t8, sm_t9, sm_t10, sm_t11, sm_t12, sm_t13, $
           varname=varnames

  sitenum = sitenuma[0:nsites-1]
  sitelon = sitelona[0:nsites-1]
  sitelat = sitelata[0:nsites-1]
  
  count=0L
  for d=0,ndepths-1 do begin
     for n=0,nsites-1 do begin
         sm(n,d,0) = sm_t1(count)
         sm(n,d,1) = sm_t2(count)
         sm(n,d,2) = sm_t3(count)
         sm(n,d,3) = sm_t4(count)
         sm(n,d,4) = sm_t5(count)
         sm(n,d,5) = sm_t6(count)
         sm(n,d,6) = sm_t7(count)
         sm(n,d,7) = sm_t8(count)
         sm(n,d,8) = sm_t9(count)
         sm(n,d,9) = sm_t10(count)
         sm(n,d,10) = sm_t11(count)
         sm(n,d,11) = sm_t12(count)
         sm(n,d,12) = sm_t13(count)
         count++
      endfor
   endfor

;  endfor

end
