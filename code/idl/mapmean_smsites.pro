;;This program maps the mean soil moisture content from each probe.
;;
;;NOTE: If it gives an error with the contour procedure,
;;    try running >.compile contour_smsites first
;;
;;kdhaynes, 2019/04

pro mapmean_smsites, idepth=idepth, psFile=psFile

  if (n_elements(idepth) eq 0) then idepth=0
  
  read_smdata_all, sitenum, sitelon, sitelat, smdata, $
         nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
         varnames=varnames
  timenames=varnames(3:ntimes+2)
  dnames=['6','18','30','42','54']
  badval=-900

  data = reform(smdata(*,idepth,*))
  meandata = fltarr(nsites)
  for i=0,nsites-1 do begin
     meandata(i) = mean(data(i,*))
  endfor
  
  ctitle = 'Soil Moisture (Volumetric)'
  title = ' Mean Soil Moisture'
  comment = string(dnames(idepth)) + ' Inches'

  contour_smsites,meandata,sitelon,sitelat, $
                     ctitle=ctitle, title=title,comment=comment, $
                     psFile=psFile
end


