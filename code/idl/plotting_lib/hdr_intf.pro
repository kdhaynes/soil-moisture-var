function hdr_intf, attr, title, title2, units, minlat, maxlat,minlon, maxlon, $
                   misval, mskval, order

size_out=size(attr)
if size_out(size_out(0)+1) ne 8 then begin
;print,'Header information not read'
	return,attr
endif else begin
		if n_elements(title) eq 0 then title=attr.ttl_str
		if n_elements(title2) eq 0 then begin
                  title2=''
                  if (strlen(attr.t_ext) GT 0) then title2=title2 + $
                     ' ' + strtrim(attr.t_ext)
                  if (strlen(attr.v_ext) GT 0) then title2=title2 + $
                     ' ' + strtrim(attr.v_ext)
                  if (strlen(attr.z_ext) GT 0) then title2=title2 + $
                     ', level ' + strtrim(attr.z_ext)
                endif
		if n_elements(units) eq 0 then units=attr.units
		if n_elements(minlat) eq 0 then $
                     minlat=attr.y_lnr.startpt - attr.y_lnr.incr * 0.5
		if n_elements(maxlat) eq 0 then $
	             maxlat=minlat + attr.y_lnr.gridpts * attr.y_lnr.incr
		if n_elements(minlon) eq 0 then $
		     minlon=attr.x_lnr.startpt - attr.x_lnr.incr * 0.5
		if n_elements(maxlon) eq 0 then $
		     maxlon=minlon + attr.x_lnr.gridpts * attr.x_lnr.incr
		if n_elements(misval) eq 0 then misval=attr.unk
		if n_elements(mskval) eq 0 then mskval=attr.msk_val
		if n_elements(order) eq 0 then order=attr.order_info


		print,'The title is ',title 
		print,'The secondary title is ',title2
		print,'Units ',units
		print,'Starting Y-coordinate',minlat
		print,'Ending Y-coordinate',maxlat
		print,'Starting X-coordinate',minlon
		print,'Ending X-coordinate',maxlon
		print,'Undefined value',misval
		print,'Mask value',mskval
		print,'Order is ',order
		size_array=size(attr.a)
		if (size_array(0) GT 2) then begin	
		  b=reform(attr.a)
		  print,size(b)
		  return,b(*,*)
		endif else begin
		  return,attr.a
		endelse
endelse
end
