pro imgsize,pos,xws,yws,xs,ys,px,py,xsc,ysc,pxc,pyc,tpos1,tpos2,orient,vmn,vmx,xmin,xmax,ymin,ymax,aspect,ts,ls,xtl,ytl,ctl,xlabel,ylabel,title,title2,units,cbar_off,tick_off,left_off,bottom_off,right,top,ytn,vbar,hbar,tlabels
;
; Calculate the position and size of the output image
;
px0=[pos(0),pos(2)]
py0=[pos(1),pos(3)]
xs0=pos(2)-pos(0)
ys0=pos(3)-pos(1)
if (xs0 LT ys0) then begin
  rfact=xs0
endif else begin
  rfact=ys0
endelse
ts=ts*(1.0 - (1.0-rfact)/1.8)
ls=ls*(1.0 - (1.0-rfact)/1.8)
if (vmx NE 0.0) then nc1 = floor(abs(alog10(abs(vmx)))) + 1 else nc1 = 3
if (nc1 LT 3) then nc1=3
if (vmx LT 0) then nc1 = nc1 + 1
if (abs(vmx) LT 1) then nc1 = nc1 + 1.5
if (vmn NE 0.0) then nc2 = floor(alog10(abs(vmn))) + 1 else nc2 = 3
if (nc2 LT 3) then nc2=3
if (vmn LT 0) then nc2 = nc2 + 1
if (abs(vmn) LT 1) then nc1 = nc1 + 1.5
nch=max([nc1,nc2])
;
if n_elements(ytn) GT 0 then begin
  ncy=max(strlen(ytn))
endif else begin
  if n_elements(tlabels) GT 0 then begin
    ncy=max(strlen(tlabels))
  endif else begin
    if (ymax NE 0.0) then nc1 = floor(alog10(abs(ymax))) + 2 else nc1 = 3
    if (nc1 LT 3) then nc1=3
    if (ymin NE 0.0) then nc2 = floor(alog10(abs(ymin))) + 2 else nc2 = 3
    if (nc2 LT 3) then nc2=3
    ncy=max([nc1,nc2])
  endelse
endelse
;
; Compute the length of the tick marks
;
if (keyword_set(tick_off)) then begin
  ctl=0.000001
endif else begin
  ctl=-0.4
endelse
if (keyword_set(tick_off)) then begin
  xtl=0.000001
  ytl=0.000001
endif else begin
  if (aspect GT 1.0) then begin
    xtl=-0.04
    ytl=-0.04/aspect
  endif else begin
    xtl=-0.04*aspect
    ytl=-0.04
  endelse
endelse
;
;  Determine character height and width.  Apply charsize.
;
xchar=convert_coord([0,!d.x_ch_size],[0,!d.x_ch_size],/device,/to_norm)
ychar_wd = xchar(0,1)
xchar_wd = xchar(1,1)
ychar=convert_coord([0,!d.y_ch_size],[0,!d.y_ch_size],/device,/to_norm)
ychar_ht = ychar(0,1)
xchar_ht = ychar(1,1)
;
; Determine spacing on the top of the plot
;
tsp = 0.5 * xchar_ht * ls
if strlen(title) GT 0 then tsp = tsp + 1.5 * xchar_ht * ts
if strlen(title2) GT 0 then tsp = tsp + 1.0 * xchar_ht * ts
if keyword_set(top) then begin
  tsp = tsp + 1.5 * xchar_ht * ls
  if NOT keyword_set(tick_off) then tsp = tsp + 1.2 * xchar_ht * ls
  if (strlen(xlabel) GT 0) then tsp = tsp + 0.5 * xchar_ht * ls
endif
tpos2=tsp+0.2*xchar_ht*ls-2.2*xchar_ht*ts*1.2
tpos1=tpos2+1.2*xchar_ht*ts
;
; Determine spacing on the bottom of the plot
;
bsp = 0.5 * xchar_ht * ls
if (NOT keyword_set(bottom_off)) then begin
  bsp = bsp + 1.5 * xchar_ht * ls
  if NOT keyword_set(tick_off) then bsp = bsp + 1.5 * xchar_ht * ls
  if (strlen(xlabel) GT 0) then bsp = bsp + 1.0 * xchar_ht * ls
endif
;
; Determine spacing on the left of the plot
;
lsp = 0.5 * ychar_ht * ls
if (NOT keyword_set(left_off)) then begin
  lsp = lsp + float(ncy) * ychar_wd * ls + 0.5 * ychar_ht * ls
  if NOT keyword_set(tick_off) then lsp = lsp + 1.2 * ychar_ht * ls
  if (strlen(ylabel) GT 0) then lsp = lsp + 1.0 * ychar_ht * ls
endif
;
; Determine spacing on the right of the plot
;
rsp = 0.5 * ychar_ht * ls
if keyword_set(right) then begin
  rsp = rsp + float(ncy) * ychar_wd * ls + 0.5 * ychar_ht * ls
  if NOT keyword_set(tick_off) then rsp = rsp + 1.2 * ychar_ht * ls
  if (strlen(ylabel) GT 0) then rsp = rsp + 1.0 * ychar_ht * ls
endif else begin
  if (NOT keyword_set(cbar_off)) then rsp = rsp + (float(nch)*ychar_wd)/2.0
endelse
;
; Compute the position of the image and the color bar
;
csize=0.03
ratio=((float(xws)*xs0*(1.0-lsp-rsp))/(float(yws)*ys0*(1.0-tsp-bsp)))/aspect
if ((NOT keyword_set(hbar)) AND (ratio GT 0.9 OR keyword_set(vbar))) then begin
  ;
  ; put on vertical color bar
  ;
  orient=1
  if (ratio LE 0.9) then begin
    ys=ratio*(ys0-tsp-bsp)/0.9
    xs=ys*float(yws)/float(xws)*aspect
  endif else begin
    ys=ys0-tsp-bsp
    xs=ys*float(yws)/float(xws)*aspect
  endelse
  ;
  ; Determine spacing for a vertical color bar
  ;
  if NOT keyword_set(cbar_off) then begin
    if (aspect GT 0.8) then cw=csize*xs else cw=csize*xs/aspect
    vsp = 2.0*cw + ychar_wd * nch * ls
    vsp = vsp + ctl * cw
    if strlen(units) GT 0 then vsp = vsp + 2.0 * ychar_ht * ls
  endif else begin
    vsp = 0.0
  endelse
  hsp = 0.0
  if (xs GT (xs0-lsp-rsp-vsp)) then begin
    xs=xs0-lsp-rsp-vsp
    ys=xs*float(xws)/float(yws)/aspect
  endif
  px=px0(0) + lsp + (xs0-xs-vsp-rsp-lsp)/2.0
  py=py0(0) + bsp + (ys0-ys-tsp-bsp)/2.0
endif else begin
  ;
  ; put on horizontal color bar
  ;
  orient=0
  if (ratio GT 0.9) then begin
    xs=0.9*(xs0-lsp-rsp)/ratio
    ys=xs*float(xws)/float(yws)/aspect
  endif else begin
    xs=xs0-lsp-rsp
    ys=xs*float(xws)/float(yws)/aspect
  endelse
  ;
  ; Determine spacing for a horizontal color bar
  ;
  if NOT keyword_set(cbar_off) then begin
    if (aspect LT 0.8) then cw=csize*ys else cw=csize*ys*aspect
    hsp = cw + 2.0 * xchar_ht * ls
    hsp = hsp - ctl * cw
    if strlen(units) GT 0 then hsp = hsp + 1.5 * xchar_ht * ls
  endif else begin
    hsp = 0.0
  endelse
  vsp = 0.0
  if (ys GT (ys0-tsp-bsp-hsp)) then begin
    ys=ys0-tsp-bsp-hsp
    xs=ys*float(yws)/float(xws)*aspect
  endif
  px=px0(0) + lsp + (xs0-xs-rsp-lsp)/2.0
  py=py0(0) + bsp + hsp + (ys0-ys-hsp-tsp-bsp)/2.0
endelse
if keyword_set(cbar_off) then begin
  xsc = xs
  ysc = ys
  pxc = px
  pyc = py
endif else begin
  if (orient EQ 1) then begin
    xsc = cw
    ysc = ys
    if (rsp GT 0.0) then begin
      pxc = px + xs + rsp
    endif else begin
      pxc = px + xs + cw
    endelse
    pyc = py
  endif else begin
    xsc = xs
    ysc = cw
    pxc = px
    pyc = py - bsp - cw
  endelse
endelse
tpos1 = tpos1 + py + ys
tpos2 = tpos2 + py + ys
;print,'Colorbar information:'
;print,'position = [',pxc,pyc,pxc+xsc,pyc+ysc,']'
;print,'ticklen  = ',ctl
;print,'units    = ',units
end
