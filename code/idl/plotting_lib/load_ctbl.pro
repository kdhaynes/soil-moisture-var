PRO load_ctbl,filename,ncolors,cmin=cmn,cmax=cmx
common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
if n_elements(cmn) eq 0 then cmn=0
if n_elements(cmx) eq 0 then cmx=255
openr, unit, filename, /get_lun, error=err
if (err ne 0) then begin
        print,'Error opening specified color table file, program exiting!'
        goto,stop
endif
readf,unit,ncolors
a=bytarr(3,ncolors)
r=bytarr(ncolors)
g=bytarr(ncolors)
b=bytarr(ncolors)
readf,unit,a
r=a(0,*)
g=a(1,*)
b=a(2,*)
nc=!d.table_size
if (nc GT ncolors) then nc=ncolors
if (nc lt ncolors) then begin ;Interpolate
  p=lindgen(nc)
  p(cmn) = (lindgen(nc-cmn) * (cmx-cmn)) / (nc-1-cmn) + cmn
  r = r(p)
  g = g(p)
  b = b(p)
endif
r_orig = r
g_orig = g
b_orig = b
r_curr = r_orig
g_curr = g_orig
b_curr = b_orig
tvlct,r, g, b
ncolors=nc
if (ncolors GT cmx) then ncolors=cmx+1
close, unit
free_lun, unit
stop:
end
