;-------------------------------------------------------------
;+
; NAME:
;       XY_IMAGE
; PURPOSE:
;       Display a color image with axis and a color bar.
; CATEGORY:
; CALLING SEQUENCE:
;       xy_image      -> Displays help information for xy_image.
;       xy_image,a
; INPUTS:
; KEYWORD PARAMETERS:
;       Keywords:
;         ASPECT=aspect  Aspect ratio for the plot (def=(xmax-xmin)/(ymax-ymin).
;         SCALE=scale    Scale factor for the input data.
;         TITLE=title    Plot title.
;         SUBTITLE=sttl  Secondary plot title.
;         UNITS=units    Units label for the color bar.
;         XLABEL=xlab    X axis label.
;         YLABEL=ylab    Y axis label.
;         VMIN=vmn       Minimum data value.
;         VMAX=vmx       Maximum data value.
;         CMIN=cmn       Color value corresponding to the minimum data value.
;         CMAX=cmx       Color value corresponding to the maximum data value.
;         XMIN=xmin      Minimum x value.
;         XMAX=xmax      Maximum x value.
;         YMIN=ymin      Minimum y value.
;         YMAX=ymax      Maximum y value.
;         NXTICK=nxt     Number of major tick mark intervals along x
;         NYTICK=nyt     Number of major tick mark intervals along y.
;         NCTICK=nct     Number of tick mark intervals for the color bar.
;         NXMINOR=mxt    Minor tick intervals per major tick along x.
;         NYMINOR=myt    Minor tick intervals per major tick along y.
;         XTICKNAME=xtn  Array of tick names for the x axis.
;         YTICKNAME=ytn  Array of tick names for the y axis.
;         TSIZE=ts       Character size for title.
;         LSIZE=ls       Character size for labels.
;         BACK_COL=bcol  Color for the background.
;         AXIS_COL=acol  Color for axis lines and labels.
;         MIS_VAL=misval The missing data value. (def=-999)
;         MSK_VAL=mskval The data value to mask out. (def=-998)
;         MIS_COL=miscol The color value for missing data. (def=0)
;         MSK_COL=mskcol The color value for mask data. (def=2)
;         POSITION=pos   Position of plot within window (def=[0,0,1,1]->whole window).
;         COL_FILE=cfile Name of the ascii color table file.
;         PS_FILE=psfile Name of the output PostScript file.
;         /MULTI         Set flag for doing multiple plots (do not set for last plot).
;         /PS_PORT       Write output to PostScript file in portrait mode.
;         /PS_LAND       Write output to PostScript file in landscape mode.
;         /BOTTOM_OFF    Turn off axis on the bottom of plot.
;         /LEFT_OFF      Turn off axis on the left side of plot.
;         /TOP           Add and label axis on top of plot.
;         /RIGHT         Add and label axis on right side of plot.
;         /TICK_OFF      Turn off the tick marks on the axis and color bar.
;         /CBAR_OFF      Turn off the color bar.
;         /VERTICAL      Force the color bar to be vertically oriented.
;         /HORIZONTAL    Force the color bar to be horizontally oriented.
;         /LABEL         Label axis E/W and N/S (default is  +/-).
;         /ORDER         Reverse order of image (top to bottom).
;         /SMOOTH        Interpolate to smooth out the image.
;         /OVERPLOT      Do Not Close Output File So User Can Overplot
; OUTPUTS:
; COMMON BLOCKS:
; MODIFICATION HISTORY:
;       W. Berg - April 3, 1995
;       L. Pakula - June, 2004 - added overplot keyword
;                              - added common block storing window
;                                position
;       J. Haynes, 2009 Oct 10 - Added ctf keyword
;
;-------------------------------------------------------------
pro xy_image, a, title=title, subtitle=sttl, units=units, xlabel=xlab, $
           ylabel=ylab, vmin=vmn, $
           vmax=vmx, cmin=cmn, cmax=cmx, xmin=xmin, xmax=xmax, $
           ymin=ymin, ymax=ymax, nxtick=nxt, nctick=nct, nytick=nyt, $
           nxminor=mxt, nyminor=myt, mis_val=misval, msk_val=mskval, $
           mis_col=miscol, msk_col=mskcol, multi=multi, ps_port=psp, $
           ps_land=psl, left_off=left_off, right=right, top=top, $
           bottom_off=bottom_off, cbar_off=cbar_off, label=label, $
           order=order, back_col=bcol, axis_col=acol, lsize=ls, $
           tsize=ts, position=pos, col_file=cfile, ps_file=psfile, $
           tick_off=tick_off, smooth=smooth, aspect=aspect, $
           xtickname=xtn, ytickname=ytn, xtickv=xtv, ytickv=ytv, $
           scale=scale, vertical=vbar, horizontal=hbar, overplot = overplot, $
           noerase=noerase, ctf=ctf, font_fix=font_fix

if (n_params(0) EQ 0) then begin
  print,' XY_IMAGE   -> Displays help information for xy_image.'
  print,' XY_IMAGE,a -> Displays a color image with axis and a color bar.'
  print,' Keywords:'
  print,'   ASPECT=aspect  Aspect ratio for the plot (def=(xmax-xmin)/(ymax-ymin).'
  print,'   SCALE=scale    Scale factor for the input data.'
  print,'   TITLE=title    Plot title'
  print,'   SUBTITLE=sttl  Secondary plot title'
  print,'   UNITS=units    Units label for the color bar'
  print,'   XLABEL=xlab    X axis label.'
  print,'   YLABEL=ylab    Y axis label.'
  print,'   VMIN=vmn       Minimum data value.'
  print,'   VMAX=vmx       Maximum data value.'
  print,'   CMIN=cmn       Color value corresponding to the minimum data value.'
  print,'   CMAX=cmx       Color value corresponding to the maximum data value.'
  print,'   XMIN=xmin      Minimum x value.'
  print,'   XMAX=xmax      Maximum x value.'
  print,'   YMIN=ymin      Minimum y value.'
  print,'   YMAX=ymax      Maximum y value.'
  print,'   NXTICK=nxt     Number of major tick mark intervals along x'
  print,'   NYTICK=nyt     Number of major tick mark intervals along y.'
  print,'   NCTICK=nct     Number of tick mark intervals for the color bar.'
  print,'   NXMINOR=mxt    Minor tick intervals per major tick along x.'
  print,'   NYMINOR=myt    Minor tick intervals per major tick along y.'
  print,'   XTICKNAME=xtn  Array of tick names for the x axis.'
  print,'   YTICKNAME=ytn  Array of tick names for the y axis.'
  print,'   TSIZE=ts       Character size for title.'
  print,'   LSIZE=ls       Character size for labels.'
  print,'   BACK_COL=bcol  Color for the background.'
  print,'   AXIS_COL=acol  Color for axis lines and labels.'
  print,'   MIS_VAL=misval The missing data value. (def=-999)'
  print,'   MSK_VAL=mskval The data value to mask out. (def=-998)'
  print,'   MIS_COL=miscol The color value for missing data. (def=0)'
  print,'   MSK_COL=mskcol The color value for mask data. (def=2)'
  print,'   POSITION=pos   Position of plot within window (def=[0,0,1,1]->whole window).'
  print,'   COL_FILE=cfile Name of the ascii color table file.'
  print,'   PS_FILE=psfile Name of the output PostScript file.'
  print,'   /MULTI         Set flag for doing multiple plots (do not set for last plot).'
  print,'   /PS_PORT       Write output to PostScript file in portrait mode.'
  print,'   /PS_LAND       Write output to PostScript file in landscape mode.'
  print,'   /BOTTOM_OFF    Turn off axis on the bottom of plot.'
  print,'   /LEFT_OFF      Turn off axis on the left side of plot.'
  print,'   /TOP           Add and label axis on top of plot.'
  print,'   /RIGHT         Add and label axis on right side of plot.'
  print,'   /TICK_OFF      Turn off the tick marks on the axis and color bar.'
  print,'   /CBAR_OFF      Turn off the color bar.'
  print,'   /VERTICAL      Force the color bar to be vertically oriented.'
  print,'   /HORIZONTAL    Force the color bar to be horizontally oriented.'
  print,'   /LABEL         Label axis E/W and N/S (default is  +/-)'
  print,'   /ORDER         Reverse order of image (top to bottom)'
  print,'   /SMOOTH        Interpolate to smooth out the image.'
  print,'   /OVERPLOT      Do Not Close Output File So User Can Overplot'

  return
endif

p = !p

;
; Read in default header values from common block
;
a2=hdr_intf(a,title,sttl,units,ymin,ymax,xmin,xmax,misval,mskval,order)
;
;-------  Set defaults  ---------------
;
if n_elements(scale) eq 0 then scale=1.0
if n_elements(title) eq 0 then title=''
if n_elements(sttl) eq 0 then sttl=''
if n_elements(units) eq 0 then units=''
if n_elements(xlab) eq 0 then xlab=''
if n_elements(ylab) eq 0 then ylab=''
if n_elements(cmn) eq 0 then cmn=5
if n_elements(cmx) eq 0 then cmx=255
if n_elements(nxt) eq 0 then nxt=6
if n_elements(nyt) eq 0 then nyt=6
if n_elements(xmin) eq 0 then xmin=0
if n_elements(xmax) eq 0 then xmax=nxt
if n_elements(ymin) eq 0 then ymin=0
if n_elements(ymax) eq 0 then ymax=nyt
if n_elements(nct) eq 0 then nct=0
if n_elements(mxt) eq 0 then mxt=0
if n_elements(myt) eq 0 then myt=0
if n_elements(misval) eq 0 then misval=-999
if n_elements(mskval) eq 0 then mskval=-998
if n_elements(miscol) eq 0 then miscol=0
if n_elements(mskcol) eq 0 then mskcol=2
if n_elements(bcol) eq 0 then bcol=255
if n_elements(acol) eq 0 then acol=0
if n_elements(ts) eq 0 then ts=1.0
if n_elements(ls) eq 0 then ls=1.0
if n_elements(pos) lt 4 then pos=[0.0,0.0,1.0,1.0]
if n_elements(cfile) eq 0 then cfile='rain.tbl'
if n_elements(psfile) eq 0 then psfile='idl.ps'
if n_elements(aspect) eq 0 then aspect=float(xmax-xmin)/float(ymax-ymin)
if n_elements(ctf) eq 0 then ctf=''
;
; Open the window or PostScript file for plotting
;
ts2=ts
ls2=ls
if keyword_set(psp) OR keyword_set(psl) then begin
  set_plot,'ps'
  load_ctbl,cfile,ncolors,cmin=cmn,cmax=cmx
  if keyword_set(psp) then begin
    if (!d.unit EQ 0) then device,filename=psfile,/COLOR,BITS_PER_PIXEL=8, $
        /PORTRAIT,xsize=6.5,ysize=9.0,xoffset=1.0,yoffset=1.0,/inches
  endif else begin
    if (!d.unit EQ 0) then device,filename=psfile,/COLOR,BITS_PER_PIXEL=8, $
        /LANDSCAPE,xsize=9.0,ysize=6.5,xoffset=1.0,yoffset=10.0,/inches
  endelse
  if (!p.multi(0) EQ 0) then begin
    tvlct,r,g,b,/get
    if (r(1) NE 255 OR g(1) NE 255 OR b(1) NE 255) then begin
      back=bytarr(10,10)+bcol
      TV,back,0.0,0.0,XSIZE=1.0,YSIZE=1.0,/norm
    endif
  endif
  xws=!d.x_size
  yws=!d.y_size
endif else begin
  if (keyword_set(noerase) eq 0) then begin
    if (!p.multi(0) EQ 0) then erase,bcol
    set_plot,'x'
  endif
  if (!d.window LT 0) then begin
    xws=600
    yws=600
    window,xsize=xws,ysize=yws
  endif else begin
    xws=!d.x_size
    yws=!d.y_size
    ts2=ts2*(1.0 - (1.0 - float(xws)/600.0)/1.8)
    ls2=ls2*(1.0 - (1.0 - float(xws)/600.0)/1.8)
  endelse
  load_ctbl,cfile,ncolors,cmin=cmn,cmax=cmx
endelse
if keyword_set(multi) then begin
  !p.multi(0)=!p.multi(0)+1
endif else begin
  !p.multi(0)=0
endelse
pmulti=!p.multi(0)
if keyword_set(order) then !order=1 else !order=0
;
; Scale the input file to byte values
;
n=size(a2)
a3=float(a2)*scale
if (n(0) NE 2) then begin
  print,'Error, user must input 2d array!'
  stop
endif 
dndex=where(a2 NE misval AND a2 NE mskval and finite(a2),dcnt)
if (dcnt GT 0) then begin
  if n_elements(vmn) eq 0 then vmn=min(a3(dndex))
  if n_elements(vmx) eq 0 then vmx=max(a3(dndex))
endif else begin
  if n_elements(vmn) eq 0 then vmn=min(a3)
  if n_elements(vmx) eq 0 then vmx=max(a3)
endelse
mndex=where(a2 EQ misval or finite(a2) EQ 0,mcnt)
index=where(a2 EQ mskval,lcnt)
cmx=cmx-(256-ncolors)
c=bytscl(a3,MIN=vmn,MAX=vmx,TOP=cmx-cmn)+cmn
if (keyword_set(smooth)) then begin
  if (mcnt GT 0) then c(mndex)=255
endif else begin
  if (mcnt GT 0) then c(mndex)=miscol
endelse
if (lcnt GT 0) then c(index)=mskcol
;
; Calculate the position and size of the output image
;
imgsize,pos,xws,yws,xs,ys,px,py,xsc,ysc,pxc,pyc,tpos1,tpos2,orient,vmn,vmx, $
  xmin,xmax,ymin,ymax,aspect,ts2,ls2,xtl,ytl,ctl,xlab,ylab,title,sttl, $
  units,cbar_off,tick_off,left_off,bottom_off,right,top,ytn,vbar,hbar
;
; Set up map coordinates
;
!P.COLOR=acol
plot,[xmin,xmax],[ymin,ymax],position=[px,py,px+xs,py+ys],xstyle=5,ystyle=5, $
  /nodata,/noerase,/normal
if keyword_set(font_fix) then begin
  xyouts,px+xs/2.0,tpos1,string(title,format='(a)'),charsize=ts2*1.5,/normal,alignment=0.5
  xyouts,px+xs/2.0,tpos2,string(sttl,format='(a)'),charsize=ts2*1.0,/normal,alignment=0.5
endif else begin
  xyouts,px+xs/2.0,tpos1,string(title,format='("!6",a)'),charsize=ts2*1.5,/normal,alignment=0.5
  xyouts,px+xs/2.0,tpos2,string(sttl,format='("!6",a)'),charsize=ts2*1.0,/normal,alignment=0.5
endelse
!P.CHARSIZE=ls2
;
; Draw the axis
;
xy_axis,nxt,nyt,xmin,xmax,ymin,ymax,xtl,ytl,xtn,ytn,xtv,ytv,xlab,ylab, $
  mxt,myt,left_off,bottom_off,label,right,top
;
; Resize and plot image
;
cc=convert_coord([px,xs],[py,ys],/normal,/to_device)
px2=cc(0,0)
py2=cc(1,0)
xs2=cc(0,1)
ys2=cc(1,1)
if keyword_set(psp) OR keyword_set(psl) then begin
  c2=c
endif else begin
  if (keyword_set(smooth)) then begin
    c2=congrid(c,xs2,ys2,interp=interp)
  endif else begin
    c2=congrid(c,xs2,ys2)
  endelse
endelse
if (dcnt GT 0) then begin
  TV,c2,px2,py2,XSIZE=xs2,YSIZE=ys2
endif else begin
  XYOUTS,(xmin+xmax)/2.0,(ymin+ymax)/2.0,'Data unavailable',ALIGNMENT=0.5,SIZE=1.4
endelse
;
; Draw box around plot
;
plot,[xmin,xmax],[ymin,ymax],/nodata,/xstyl,/ystyl,xticks=1,xtickn=[' ',' '],yticks=1,ytickn=[' ',' '],position=[px,py,px+xs,py+ys],/noerase
!p.multi(0)=pmulti
;
; Draw the colorbar
;
!order=0
if (NOT keyword_set(cbar_off)) then begin
  if (orient EQ 1) then begin
    colorbar,/vertical,/right,vmin=vmn,vmax=vmx,cmin=cmn,cmax=cmx, $
      ticklen=ctl,position=[pxc,pyc,pxc+xsc,pyc+ysc],title=units, $
      nticks=nct,charsize=ls2,tf=ctf
  endif else begin
    colorbar,/horizontal,/bottom,vmin=vmn,vmax=vmx,cmin=cmn,cmax=cmx, $
    ticklen=ctl,position=[pxc,pyc,pxc+xsc,pyc+ysc],title=units, $
    nticks=nct,charsize=ls2,tf=ctf
  endelse
endif
;
; Close the output PostScript file
;
if (keyword_set(psp) OR keyword_set(psl)) AND (!p.multi(0) LE 0) then begin
    if not keyword_set(overplot) then begin
  device,/close_file
  set_plot,'x'
  endif
endif

if keyword_set(font_fix) then !p = p

common winpos, winpos_ps, winpos_screen
winpos_ps     = [px,py,px+xs,py+ys]
winpos_screen = [px2,py2,px2+xs2,py2+ys2]

end
