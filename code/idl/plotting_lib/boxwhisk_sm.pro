;;Program to plot a box and whisker diagram
;;   Expects a single array to be overplot an existing figure.
;;NOTE - DOES NOT PLOT OUTLIERS!!
;;
;;kdhaynes, 2018/09

pro boxwhisk_sm, data, xlocation=xlocation, width=width, $
    outlinecolor=outlinecolor, fillcolor=fillcolor

if (n_elements(xlocation) eq 0) then xlocation=0
if (n_elements(width) eq 0) then width=0.3
if (n_elements(outlinecolor) eq 0) then outlinecolor='black'
if (n_elements(fillcolor) eq 0) then fillcolor='white'

mminval=-900
mypercentiles=[0.25,0.5,0.75]
mfsize=20
ofsize=8

minimum=min(data)
maximum=max(data)
median=median(data)
pciles=cgPercentiles(data,Percentiles=mypercentiles)
lquartile=pciles(0)
uquartile=pciles(2)
;iqrange=(uquartile-lquartile)*1.5
iqrange=maximum-minimum

llbottom=lquartile-iqrange
IF (min(data) lt llbottom) THEN BEGIN
   sortref=sort(data)
   tempdata = data(sortref)
   goodref = where(tempdata gt llbottom)
   minimum=tempdata(goodref[0])
ENDIF ELSE BEGIN
   minimum=min(data)
ENDELSE

lltop=uquartile+iqrange
IF (max(data) gt lltop) THEN BEGIN
   sortref=sort(data)
   tempdata = data(sortref)
   goodref = where(tempdata lt lltop)
   nn=n_elements(goodref)
   maximum=tempdata(goodref(nn-1))
ENDIF ELSE BEGIN
  maximum=max(data)
ENDELSE
tarr=[minimum,lquartile,median,uquartile,maximum]

xarrayl=xlocation-width
xarrayr=xlocation+width
coords=[[xarrayl,tarr(1)],[xarrayr,tarr(1)], $
        [xarrayr,tarr(3)],[xarrayl,tarr(3)]]
ooplot=polygon(coords, /data, /overplot, $
          color=outlinecolor, fill_color=fillcolor)

myxvals=[xarrayl,xarrayr]
myyvals=[tarr(2),tarr(2)]
medplot=plot(myxvals,myyvals,/data,/overplot)

myxvals=[xlocation,xlocation]
myyvals=[tarr(0),tarr(4)]
wplot=plot(myxvals,myyvals,/data,/overplot)

myxvals=[xlocation-0.05,xlocation+0.05]
myyvals=[tarr(0),tarr(0)]
wlowplot=plot(myxvals,myyvals,/data,/overplot)

myyvals=[tarr(4),tarr(4)]
whighplot=plot(myxvals,myyvals,/data,/overplot)

;are there any low outliers?
outref = where(data lt tarr(0))
if (outref(0) ge 0) then begin
   nn=n_elements(outref)
   for n=0,nn-1 do begin
      otext=text(xlocation,data(outref(n)), /data, $
           'o', alignment=0.5, vertical_alignment=0.5, $
           font_size=ofsize)
   endfor
endif

;are there any high outliers?
outref = where(data gt tarr(4))
if (outref(0) ge 0) then begin
   nn=n_elements(outref)
   for n=0,nn-1 do begin
       utext=text(xlocation,data(outref(n)), /data, $
           'o', alignment=0.5, vertical_alignment=0.5, $
           font_size=ofsize)
    endfor
endif

;plot the mean?
;print,'Site Mean:',min(mean(data))
mtext=text(xlocation,mean(data), /data, $
           'o', alignment=0.5, vertical_alignment=0.5, $
           font_size=mfsize, color=outlinecolor)

end
