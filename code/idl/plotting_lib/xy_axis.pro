; Variables used...
; nxt --- Number of major tick mark intervals along x
; nyt --- Number of major tick mark intervals along y
; xtv --- Number of major tick marks along x 
; ytv --- Number of major tick marks along y
; minlon ---
; maxlon ---
; minlat ---
; maxlat ---
; xtl --- (xticklength)
; ytl --- (yticklength)
; xtn --- (xtickname)
; ytn --- (ytickname)
; xlab -- (xlabel)
; ylab --- (ylabel)
; mxt --- (xminor) minor tick intervals per major tick interval along x
; myt --- (yminor) minor tick intervals per major tick interval along y
; xtn --- labels for xaxis for both minor ticks and major ticks 
; ytn --- labels for yaxis
; left_off --- take out the left axis
; bottom_off ---  take out the bottom axis
; label --- to label E/W and N/S
; right -- add and label axis on right side of plot
; top --- add and label axis on top of the plot

pro xy_axis,nxt,nyt,minlon,maxlon,minlat,maxlat,xtl,ytl,xtn,ytn,xtv,ytv,xlab,ylab,mxt,myt,left_off,bottom_off,label,right,top


;
; Draw the axis
;
if (keyword_set(top)) then begin
  if (nxt GT 0) then begin
    if n_elements(xtv) EQ 0 then begin
      xtv=fltarr(nxt+1)
      for i=0,nxt do xtv(i) = minlon + i*(maxlon-minlon)/nxt
    endif
    if n_elements(xtn) GT 0 then begin
      axis,xaxis=1,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],$
           xstyle=9,xticks=nxt,xminor=mxt,xtickv=xtv,xtickname=xtn
    endif else begin
      axis,xaxis=1,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],$
           xstyle=9,xticks=nxt,xminor=mxt,xtickv=xtv
    endelse
  endif else begin
    axis,xaxis=1,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],xstyle=9
  endelse
endif
if (NOT keyword_set(bottom_off)) then begin
  if (nxt GT 0) then begin
    if n_elements(xtv) EQ 0 then begin
      xtv=fltarr(nxt+1)
      for i=0,nxt do xtv(i) = minlon + i*(maxlon-minlon)/nxt
    endif
    if n_elements(xtn) GT 0 then begin
      axis,xaxis=0,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],$
           xstyle=9,xticks=nxt,xminor=mxt,xtickv=xtv,xtickname=xtn
    endif else begin
      axis,xaxis=0,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],$
           xstyle=9,xticks=nxt,xminor=mxt,xtickv=xtv
    endelse
  endif else begin
    axis,xaxis=0,xticklen=xtl,xtitle=xlab,xrange=[minlon,maxlon],xstyle=9
  endelse
endif
if (NOT keyword_set(left_off)) then begin
  if (nyt GT 0) then begin
    if n_elements(ytv) EQ 0 then begin
      ytv=fltarr(nyt+1)
      for i=0,nyt do ytv(i) = minlat + i*(maxlat-minlat)/nyt
    endif
    if n_elements(ytn) GT 0 then begin
      axis,yaxis=0,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],$
           ystyle=9,yticks=nyt,yminor=myt,ytickv=ytv,ytickname=ytn
    endif else begin
      axis,yaxis=0,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],$
           ystyle=9,yticks=nyt,yminor=myt,ytickv=ytv
    endelse
  endif else begin
    axis,yaxis=0,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],ystyle=9
  endelse
endif
if (keyword_set(right)) then begin
  if (nyt GT 0) then begin
    if n_elements(ytv) EQ 0 then begin
      ytv=fltarr(nyt+1)
      for i=0,nyt do ytv(i) = minlat + i*(maxlat-minlat)/nyt
    endif
    if n_elements(ytn) GT 0 then begin
      axis,yaxis=1,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],$
           ystyle=9,yticks=nyt,yminor=myt,ytickv=ytv,ytickname=ytn
    endif else begin
      axis,yaxis=1,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],$
           ystyle=9,yticks=nyt,yminor=myt,ytickv=ytv
    endelse
  endif else begin
    axis,yaxis=1,yticklen=ytl,ytitle=ylab,yrange=[minlat,maxlat],ystyle=9
  endelse
endif

end
