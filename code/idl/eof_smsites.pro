;;Program to calculate EOF and PC analysis
;;   Plots results
;;
;;kdhaynes, 2019/05

pro eof_smsites, psFile=psFile

  idepth=1 ;0-4
  read_smdata_all, sitenum, sitelon, sitelat, smdata, $
              nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
              varnames=varnames

  
  timenames=varnames(3:ntimes+2)
  dnames=['6','18','30','42','54']
  badval=-900

  ctitle = 'Soil Moisture'
  title = ' 2012 At Depth ' $
          + dnames(idepth) + ' Inches'
  
  minlat=40.6644
  maxlat=40.66600
  minlon=-104.9998
  maxlon=-104.9960
  mylimit = [minlat,minlon,maxlat,maxlon]
  
  ;remove all nodes with missing values
  goodnode = fltarr(nsites) + 1
  for i=0,nsites-1 do begin
      for t=0,ntimes-1 do begin
         if (smdata(i,idepth,t) lt 0) then goodnode[i] = 0
      end
  endfor
  nnodes = total(goodnode)
  print,'Number of good nodes: ',nnodes

  goodnode = fltarr(nsites) + 1
  dlat = fltarr(nnodes)
  dlon = fltarr(nnodes)
  data = fltarr(nnodes,ntimes)
  count = 0
  for i=0,nsites-1 do begin
     for t=0,ntimes-1 do begin
        if (smdata(i,idepth,t) lt 0) then goodnode(i) = 0
     endfor

     if (goodnode(i) eq 1) then begin
        data(count,*) = smdata(i,idepth,*)
        dlat(count) = sitelat(i)
        dlon(count) = sitelon(i)
        count++
     endif
  endfor

  ;Save data
  ;save, filename='smdata_all.sav', data, /compress
  ;save, filename='smdata_all.sav', sitenum, sitelon, sitelat, smdata
  
  ;Data anomalies
  sm_anomalies = data
  FOR j=0,nnodes-1 DO sm_anomalies[j,*] = data[j,*] - mean(data[j,*])
  ;sm_anomalies *= -1.
  ;contour_smsites,sm_anomalies[*,0], dlon, dlat, limit=mylimit, $
  ;         ctitle=ctitle, title='SM Anomalies'

  ;Covariance matrix
  matrix = (1/ntimes-1) * (double(sm_anomalies) ## Transpose(sm_anomalies))

  ;Calculate eigenvalues and eigenvectors of covariance matrix
  LA_SVD, matrix, W, U, V
  ;plot,v(0,*),xrange=[0,12],/xstyle

  dims = size(sm_anomalies,/Dimensions)
  eof = fltarr(dims[1],dims[0])
  FOR j=0,dims[1]-1 DO BEGIN
     t = transpose(sm_anomalies) ## U[j,*]
     eof[j,*] = t/sqrt(total(t^2))
  ENDFOR

  ;Percent Variance
  percent_variance = W / Total(W) * 100.0
  print,'Percent Variance: ',percent_variance
  
  ;Principal components
  pc = fltarr(dims[1], dims[1])
  FOR j=0,dims[1]-1 DO pc[j,*] = sm_anomalies ## eof[j,*]

  close_windows
  mytitle='Principal Component (Mode 1 at ' + dnames[idepth] + ' in)'
  myplot = plot(pc[0,*], xrange=[0,12], /xstyle, $
                font_size=11, font_name='Hershey', $
                title=mytitle, $
                ytitle='Amplitude', xticklen=0.02, $
                margin=[0.12,0.06,0.04,0.08], $
                xtickname=timenames, xminor=0)
  myplot.title.font_name='Hershey'
  myplot.title.font_size=12

  pvartext = '% Var = ' + string(percent_variance[0],format='(f6.2)')
  mytext = text(0.24,0.84,pvartext,/normal,vertical_alignment=0.5, $
               align=0.5,font_size=11)
 
  ;Plot the EOF analysis
  mode = 1
  theEOF = reform(eof[mode-1,*], nnodes)

  comment = 'Depth: ' + dnames[idepth] + ' inches'
  contour_smsites,theEOF, dlon, dlat, limit=mylimit, $
         ctitle=ctitle, title='EOF of Mode 1', comment=comment, $
         psFile=psFile         

  
end


