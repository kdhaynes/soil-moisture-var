;;This program creates a map of the field
;;    with the soil moisture probe locations.
;;
;;kdhaynes, 2019/04

pro map_smsites, title=title, psFile=psFile

  filename='../../data/latlon.csv'
  read_csv_simpro,filename,sitenum,sitelon,sitelat,varname=varname

  minlat=40.6644
  maxlat=40.66600
  minlon=-105.0000
  maxlon=-104.9960
  limit=[minlat,minlon,maxlat,maxlon]
  
  csize=2.2
  cthick=2.8

  set_display, psFile=psFile
  map_set, /mercator, /isotropic, limit=[minlat,minlon,maxlat,maxlon]
  map_grid
  
  if (n_elements(title) gt 0) then begin
       xyouts,0.5,0.85,/normal,'Soil Moisture Sites',size=4.,align=0.5
  endif
       
  nsites=n_elements(sitenum)
  for i=0,nsites-1 do begin
     mystring=string(sitenum(i),format='(I2)')
     xyouts,sitelon(i),sitelat(i),mystring,color=240, $
            align=0.5, charsize=csize, charthick=cthick
  endfor

  if (n_elements(psFile) gt 0) then device,/close
  
end
