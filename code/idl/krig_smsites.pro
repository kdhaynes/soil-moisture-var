;;This program performs kriging on the soil moisture data,
;;  and creates resulting contour plot.
;;
;;kdhaynes, 2019/04

pro krig_smsites

  pbdepth = 0 ;0-4
  pbtime = 0  ;0-12
  
  read_smdata, sitenum, sitelon, sitelat, smdata, $
               nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
               varnames=varnames
  timenames=varnames(3:ntimes+2)

  data = smdata(*,pbdepth,pbtime)
  interpArray = cgKrig2D(data, sitelon, sitelat, $
                         SPHERICAL=[0.0006, 0.0003, 0.0015], $
                         xout=xout, yout=yout)
  cgSurf, interpArray, xout, yout, /Save
  cgPlots, sitelon, sitelat, data, PSYM=2, Color='red', /T3D

  cgContour, interpArray, xout, yout, /fill
  
end
