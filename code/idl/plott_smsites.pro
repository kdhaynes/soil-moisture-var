;;This program plots time-series for the soil moisture probes.
;;  --Contains various plotting and depth options
;;
;;kdhaynes, 2019/04

pro plott_smsites

  idepth=4   ;0-4     
  plottsd=-1 ;1=yes
  plotall=1 ;1=yes
  plottvt1=-1

  read_smdata, sitenum, sitelon, sitelat, smdata, $
               nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
               varnames=varnames
  timenames=varnames(3:ntimes+2)
  dnames=['6','18','30','42','54']
  dcols=['black','purple','blue','orange','red']
  tcols=['black','midnight blue','medium blue','medium slate blue', $
         'blue violet','medium purple','medium orchid','pale violet red', $
         'indian red','orange red','dark orange','orange', $
         'sienna']
  ytitle = 'Soil Moisture'
  ;title = ' Depth ' + dnames(idepth) + ' Inches'
  title = 'All Sites, All Times, All Depths'
  badval=-900

  time=findgen(ntimes)
  close_windows

  if (plottsd eq 1) then begin
      data = fltarr(nsites,ntimes)
      data(*,*) = smdata(*,idepth,*)
     
      pplot = plot(time,data(0,*), $
               margin=[0.1,0.1,0.04,0.1], $
               /xstyle,/ystyle, $
               xtickname=timenames, xtitle='Date', $
               ytitle=ytitle,title=title)
      pplot.title.font_size=12
      pplot.title.font_name='Hershey'

      for s=0,nsites-1 do begin
         oplot = plot(time,data(s,*), $
                    /overplot, min_val=badval)
      endfor
   endif

  if (plotall eq 1) then begin
     ymin=0.1
     ymax=max(smdata)*1.1
     pplot = plot(time,smdata(0,0,*),/nodata, $
               margin=[0.1,0.1,0.04,0.1], $
               /xstyle,/ystyle,yrange=[ymin,ymax], $
               xtickname=timenames, xtitle='Date', $
                  ytitle=ytitle,title=title)
     pplot.title.font_size=12
     pplot.title.font_name='Hershey'
           
     for s=0,nsites-1 do begin
        for d=1,ndepths-1 do begin
           oplot = plot(time,smdata(s,d,*),/overplot, $
                        min_val=badval,color=dcols(d))
        endfor
     endfor

     ;plot label
     xval=0.12
     yval=0.84
     xint=0.14
     yint=0.00
     for d=0,ndepths-1 do begin
        mytext = dnames(d) + ' Inches'
        stext=text(xval+d*xint,yval-d*yint,mytext,/normal,color=dcols(d))
     endfor
  endif ;plotall

  if (plottvt1 eq 1) then begin
     ymin=0.1
     ymax=0.5
     myytitle=ytitle + ' at T+1'
     myxtitle=ytitle+ ' at T'
     pplot = plot(smdata(*,idepth,0),smdata(*,idepth,1), $
                  /nodata, margin=[0.1,0.1,0.1,0.1], $
                  xtitle=myxtitle,ytitle=myytitle, $
                  /xstyle,/ystyle,title=title, $
                  xrange=[ymin,ymax], yrange=[ymin,ymax])
     pplot.title.font_size=14
     pplot.title.font_name='Hershey'
     
     for t=0,ntimes-2 do begin
        oplot=plot(smdata(*,idepth,t),smdata(*,idepth,t+1), $
                   /overplot, $
                   min_val=min_val,symbol='circle', $
                   linestyle=6, color=tcols(t), $
                   sym_filled=1, sym_size=0.8)
     endfor

     ;plot label
     xval=0.95
     yval=0.84
     xint=0.00
     yint=0.06
     for t=0,ntimes-2 do begin
        stext=text(xval+t*xint,yval-t*yint,timenames(t), $
                   color=tcols(t),align=0.5)
     endfor
  endif  ;plottvt1
end
