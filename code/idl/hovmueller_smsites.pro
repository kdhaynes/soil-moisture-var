;;This program reads the soil moisture data AND
;;
;;  - Creates a count of the volumetric soil moisture distribution
;;  - Saves the image histrogram_all.pdf
;;
;;  - Creates hovmueller diagrams of all soil moisture levels
;;  - Saves images in files called structurexx.eps
;;
;;  - There are spawn commands for scripts to combine the
;;    individual eps files, uncomment to use if you have the scripts
;;
;;In order to run the programs, either export the plotting_lib
;;   directory or copy those files into the directory with this file.
;;
;;kdhaynes, 2019/04

pro hovmueller_smsites

; data setup

  nstation = 41
  nlevel = 5
  ntime = 13
  data = fltarr(nstation,nlevel,ntime)

; read data

  datafile = '../../data/Vol_AllF.csv'
  nline = file_lines(datafile)
  data_str = ''
  openr, 2, datafile  
  readf, 2, data_str
  tmp = strsplit(data_str,',')
  for i=0L,nlevel-1 do begin
    for j=0L,nstation-1 do begin
      readf, 2, data_str
      tmp = strsplit(data_str,',',/extract)
      data[j,i,*] = float(tmp[3:*])
    endfor
  endfor
  close, 2

; plot histogram of all data

  !p.thick=1.5
  !p.charsize=1.5
  !p.charthick=1.5
  set_plot, 'ps'
  device, filename='histogram_all.eps', /encapsulated
  res = histogram(data,locations=loc,binsize=0.02)
  plot, loc, res, $
    xtitle='Soil moisture', $
    ytitle='Count'
  device, /close

; plot each level

  for i=0L,nlevel-1 do begin
    fname = 'structure'+strtrim(i+1,2) +'.eps'
    xy_image, ps_file=fname, /ps_port, $
      /font_fix, $
      reform(data[*,i,*]), $
      vmin=min(data), vmax=max(data), $
      nxtick=11, nytick=13, aspect=1.25, $
      xmin=0, xmax=41, $
      cmax=250, $
      /vertical, $
      tsize=1.5, $
      title='Soil Moisture of Level '+strtrim(i+1,2), $
      lsize=1.25, $
      xlabel='Station number', ylabel='Time Dimension'
    ;spawn, 'ps2eps ' + fname  
  endfor

  ;spawn, 'perl ps_combine.pl -n 2 -h 15 -v 3 -o structure.eps s*.eps.eps'
  ;spawn, 'rm structure?.eps structure?.eps.eps'  

  end
  
