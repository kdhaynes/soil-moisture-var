;;This program maps the cluster colors for each soil moisture probe.
;;
;;kdhaynes, 2019/04

pro mapclust_smsites, reftoplot=mypref, psFile=psFile

    if (n_elements(mypref) eq 0) then mypref=0
    read_smdata_all, sitenum, sitelon, sitelat, smdata, $
         nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
         varnames=varnames

      clust  =  [[3,3,3,1,3,3,2,2,3,3,3,3,3,3,3,2,1,2,2,1,1,1,3,1,1,1,1,1, $
              3,1,3,3,1,1,3,1,1,2,2,2,1], $
             [2,2,2,1,4,4,3,3,4,4,2,2,2,2,2,3,5,3,3,1,1,5,2,1,1,1,1,1, $
              2,5,4,2,1,1,2,5,5,3,3,3,4], $
             [5,5,5,2,4,4,1,4,4,4,8,8,8,8,5,7,6,7,7,2,2,6,8,3,3,3,3,3,8, $
              2,3,8,2,2,8,2,6,1,7,1,3], $
             [12,12,12,1,5,5,9,11,5,5,8,8,2,8,12,10,4,11,10,1,1,7,8,6,6, $
              6,6,6,8,1,6,8,1,1,8,1,7,3,11,3,6], $
             [3,1,3,2,1,2,3,3,3,2,2,2,2,2,2,2,2,1,2,3,2,3,2,3,3,3,2,3,2,2, $
              2,2,3,3,3,3,2,3,3,2,2], $
             [1,5,2,2,5,2,4,4,4,2,2,2,3,3,5,2,2,5,3,4,3,4,3,1,4,1,3,1,2, $
              1,3,3,4,1,4,4,1,1,4,2,1], $
             [5,8,3,8,6,8,3,3,3,8,8,3,1,1,8,7,8,6,7,2,1,5,1,4,4,4,1,7,8, $
               7,1,1,5,5,3,5,7,5,5,8,7], $
             [1,5,9,9,12,6,7,7,7,5,9,9,10,10,5,11,6,12,11,8,10,4,10,2,3,2, $
             10,1,9,11,10,10,7,1,7,7,11,1,4,5,11], $ 
             [2,1,1,1,3,1,2,2,2,1,1,1,1,1,1,1,1,3,1,2,1,2,1,1,2,1,3,1,3,1, $
              1,1,2,1,1,2,2,2,2,1,1], $
             [2,5,1,1,4,1,3,2,3,3,1,1,5,5,1,1,5,4,5,3,5,3,5,5,3,5,4,3,4,5, $
              5,5,3,5,3,3,3,2,3,1,5], $
             [6,5,1,1,7,1,8,6,4,4,4,4,5,5,1,4,5,2,3,8,5,3,5,5,8,4,7,4,7,4, $
               5,5,8,5,4,8,8,6,8,1,5], $
             [4,11,3,3,6,3,9,9,10,10,12,10,11,11,3,12,11,8,5,1,11,2,11,11, $
             1,10,7,10,7,10,11,7,1,11,10,1,1,4,1,3,11], $
             [2,2,2,1,1,1,3,3,2,2,1,2,2,2,2,2,3,2,2,2,1,2,1,1,2,2,1,2,1, $
               2,1,1,2,1,2,2,2,3,2,1,1], $
             [4,4,1,5,5,5,2,4,1,1,1,1,1,4,4,1,4,1,4,4,3,1,3,3,1,1,3,1,3,1,$
              3,3,1,3,1,1,4,2,1,5,3], $
             [ 8,8,6,6,5,5,2,8,6,3,6,3,3,8,8,1,8,1,1,3,7,1,7,7,3,3,4,3,4, $
               3,7,7,3,7,6,3,8,2,3,5,7], $
             [7, 7,1,2,9,9,11,3,1,1,2,1,10,7,7,6,3,6,5,7,8,5,8,8,10,10,4, $
              10,4,10,8,8,10,8,1,10,7,12,10,9,8], $
             [1,1,2,3,3,3,1,1,3,3,3,2,1,1,1,1,1,1,1,2,2,2,2,3,2,2,3,2,3, $
              2,3,3,2,2,2,1,1,1,1,3,3], $
             [2,2,3,4,4,1,5,2,4,4,4,3,2,2,2,2,2,2,2,3,3,3,3,4,3,3,4,3,4, $
              3,4,4,3,3,2,2,2,5,2,4,4], $
             [ 3,2,5,7,7,8,6,2,7,7,7,5,3,2,3,3,2,3,2,5,1,1,1,4,5,1,4,5, $
               4,1,4,4,5,5,3,2,3,6,2,7,4], $
             [1,2,10,12,8,9,4,2,8,8,12,10,11,2,1,1,2,1,2,10,3,5,3,6,11,3,7,$
              11,7,3,7,7,10,11,11,2,1,4,2,12,6], $
           [1,1,2,3,3,3,1,1,2,3,3,2,1,1,1,1,1,3,1,2,3,2,3,3,2,2, $
             3,2,3,2,3,3,2,2,2,2,1,1,1,3,3], $
             [2,5,3,3,4,4,1,1,2,3,3,2,5,5,5,5,5,5,5,2,3,2,3,3,2,3,3, $
              2,3,3,3,3,2,3,2,2,2,1,2,4,3],  $
             [5,6,1,4,8,8,3,3,1,1,1,1,6,6,6,2,6,2,2,7,4,7,4,4,7,7, $
              4,7,4,4,4,4,7,4,7,5,5,3,5,8,4], $
             [6,7,5,9,10,12,1,1,5,5,5,5,7,7,7,3,7,8,3,11,9,11,2,9,11,9,2,11,  $
              2,9,2,2,11,9,11,6,6,4,6,10,9]]
       comment = ['6 Inches, 3 Clusters', $
             '6 Inches, 5 Clusters', $
             '6 Inches, 8 Clusters', $
             '6 Inches, 12 Clusters', $
             '18 Inches, 3 Clusters', $
             '18 Inches, 5 Clusters', $
             '18 Inches, 8 Clusters', $
             '18 Inches, 12 Clusters', $
             '30 Inches, 3 Clusters', $
             '30 Inches, 5 Clusters', $
             '30 Inches, 8 Clusters', $
             '30 Inches, 12 Clusters', $
             '42 Inches, 3 Clusters', $
             '42 Inches, 5 Clusters', $
             '42 Inches, 8 Clusters', $
             '42 Inches, 12 Clusters', $
             '54 Inches, 3 Clusters', $
             '54 Inches, 5 Clusters', $
             '54 Inches, 8 Clusters', $
             '54 Inches, 12 Clusters', $
             'All Depths, 3 Clusters', $
             'All Depths, 5 Clusters', $
             'All Depths, 8 Clusters', $
             'All Depths, 12 Clusters']

   ctitle = 'Clusters'
   title = 'Soil Moisture Clusters'
  
   if (mypref lt 0) then myloop=4*ndepths+2 else myloop=0
   for i=0,myloop do begin
      if (mypref < 0) then begin
           myfout = 'smclust'+string(i,format='(I02)')+'.eps'
           psFile=myfout
           mref=i
       endif else mref=mypref

      data=reform(clust[*,mref])
      comm=comment[mref]
      contours=findgen(max(data)-min(data)+1)+0.5
      contour_clusters,data,sitelon,sitelat, $
      ctitle=ctitle, title=title,comment=comm, $
                       psFile=psFile, contours=contours
   endfor
end


;;This program plots up a map of soil moisture data.
;;
;;kdhaynes, 2019/04

pro contour_clusters,data,lon,lat,contours=contours,limit=limit, $
    title=title, ctitle=ctitle, psFile=psFile, comment=comment

dsize=size(data)
dsize=dsize(1)
if (n_elements(limit) eq 0) THEN BEGIN
   minlat=40.6643
   maxlat=40.66600
   minlon=-105.0001
   maxlon=-104.9960
   limit=[minlat,minlon,maxlat,maxlon]
ENDIF

deltai=0.0001
deltaj=0.0001

if (n_elements(psFile) ne 0) then begin
    set_plot,'PS'
    !p.font=0
    myheight=4.5
    mywidth=7.
    device,filename=psFile,xsize=mywidth,ysize=myheight,/inches, $
        color=8,font_size=6,/encapsulated,bits_per_pixel=8
endif else begin
   set_display
endelse
set_plot_colors,scheme=3

;.....
; Set up arrays to hold grid coordinates
missing_value=-999.99
latNW = FLTARR(dsize)-missing_value
lonNW = FLTARR(dsize)-missing_value
latNE = FLTARR(dSize)-missing_value
lonNE = FLTARR(dSize)-missing_value
latSE = FLTARR(dSize)-missing_value
lonSE = FLTARR(dSize)-missing_value
latSW = FLTARR(dSize)-missing_value
lonSW = FLTARR(dSize)-missing_value

for i=0L,dsize-1 do begin
   if (lat(i) ne -999.99 and lon(i) ne missing_value) then begin
     latNW(i) = lat(i)+deltaj
     if (latNW(i) gt 90.) then latNW(i)=90.
     lonNW(i) = lon(i)-deltai
     if (lonNW(i) gt 180) then lonNW(i)=180.
     latNE(i) = lat(i)+deltaj
     if (latNE(i) gt 90) then latNE(i)=90. 
     lonNE(i) = lon(i)+deltai
     if (lonNE(i) gt 180) then lonNE(i)=180
     latSW(i) = lat(i)-deltaj
     if (latSW(i) lt -90) then latSW(i)=-90
     lonSW(i) = lon(i)-deltai
     if (lonSW(i) lt -180) then lonSW(i)=-180
     latSE(i) = lat(i)-deltaj
     if (latSE(i) lt -90) then latSE(i)=-90
     lonSE(i) = lon(i)+deltai
     if (lonSE(i) lt -180) then lonSE(i)=-180
    endif
endfor

;Set up contour levels based on max and min of data
if (n_elements(contours) eq 0) then begin
 nlevels=12
 gooddata = where(data ne missing_value)
 gooddata = data(gooddata)
 plotmin = min(gooddata)
 plotmax = max(gooddata)
 interval = (plotmax-plotmin)/(nlevels+1)
 contours = plotmin + findgen(nlevels)*interval
endif else begin
 nlevels=size(contours)
 nlevels=nlevels(1)
endelse

;Set up color table to make plot
nColors=255
;loadct,39220
;colorFactor = nColors/(nlevels+1)
mycolors=[200,30,60,220,240,132,85,230,180,145,113,15] ;,100,190,55,115,85,10]
scaleddata = bytarr(dsize)
for level = 0L, nlevels-1 do begin
    points = where(data ge contours[level],npoints)
    if npoints ne 0 then scaleddata[points] = byte(mycolors[level])
endfor


;Set map coordinates
map_set,/mercator,/isotropic,position=[.1,.1,.9,.9], $
        limit=limit
xyouts, 0.5, 0.95, title, /normal, charsize=2.4, align=0.5
xyouts, 0.5, 0.90, comment, /normal, charsize=1.6, align=0.5
map_grid

;Fill in grid boxes with color according to value of data
for i=0L,dsize-1 do begin
   if (data[i] ne missing_value) then begin
      if (scaleddata[i] eq 0) then scaleddata[i]=255B
      polyfill,color=scaleddata[i], $
          [lonNW[i],lonNE[i],lonSE[i],lonSW[i]], $
          [latNW[i],latNE[i],latSE[i],latSW[i]]
   endif
endfor

;Add cluster info
xlev=0.12
xint=0.18
ylev=0.12
yint=0.04
myxlev=[xlev,xlev+xint,xlev+2*xint,xlev+3*xint, $
        xlev,xlev+xint,xlev+2*xint,xlev+3*xint, $
        xlev,xlev+xint,xlev+2*xint,xlev+3*xint]
myylev=[ylev,ylev,ylev,ylev, $
        ylev-yint,ylev-yint,ylev-yint,ylev-yint, $
        ylev-yint*2,ylev-yint*2,ylev-yint*2,ylev-yint*2, $
        ylev-yint*3,ylev-yint*3.,ylev-yint*3,ylev-yint*3]
for i=1, nlevels do begin
    if (i < 10) then myform='(I2)' $
    else myform='(I3)'
     mystring = 'Cluster ' + string(i,format=myform)
     xyouts,myxlev[i-1],myylev[i-1],mystring,/normal, $
            color=mycolors[i-1], charsize=1.6
endfor
 
;color_bar,contours,1,labelFormat='(I4)'  
if (n_elements(psFile) gt 0) then begin
   device, /close
   set_plot, 'X'
endif

end

