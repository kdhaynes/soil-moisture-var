;; Plot descriptive statistics of soil moisture data.
;;   -- By depth
;;   -- By site
;;   -- By time
;;
;;   -- Uses box-whisker diagrams in boxwhisk_sm.pro
;;        IF this gives you an error, compile that first
;;
;;kdhaynes, 2019/04

pro stats_smsites

  pbdepth = 1
  pbsite = 1
  pbtime = 1
  
  dnames=['6','18','30','42','54']
  dlabel='Inches'
  slabel='Soil Moisture (Volumetric)'

  titlepd='Soil Moisture Stats By Depth'
  titleps='Soil Moisture Stats By Site'
  titlept='Soil Moisture Stats By Time'
  badval=-900

  ; Read in data
  read_smdata_all, sitenum, sitelon, sitelat, smdata, $
               nsites=nsites, ndepths=ndepths, ntimes=ntimes, $
               varnames=varnames
  timenames=varnames(3:ntimes+2)
  close_windows

  ; Print stats:
  print,'Min: ',min(smdata)
  print,'Max: ',max(smdata)
  
  if (pbdepth) then begin
     ; Pull out all data by depth
     datad1 = fltarr(nsites*ntimes)
     datad2 = fltarr(nsites*ntimes)
     datad3 = fltarr(nsites*ntimes)
     datad4 = fltarr(nsites*ntimes)
     datad5 = fltarr(nsites*ntimes)
  
     count1=0
     count2=ntimes-1
     for s=0,nsites-1 do begin
         datad1(count1:count2) = smdata(s,0,*)
         datad2(count1:count2) = smdata(s,1,*)
         datad3(count1:count2) = smdata(s,2,*)
         datad4(count1:count2) = smdata(s,3,*)
         datad5(count1:count2) = smdata(s,4,*)
      
         count1+=ntimes
         count2+=ntimes
     endfor
     goodref=where(datad1 gt badval)
     datad1 = datad1(goodref)
     goodref=where(datad2 gt badval)
     datad2 = datad2(goodref)
     goodref=where(datad3 gt badval)
     datad3 = datad3(goodref)
     goodref=where(datad4 gt badval)
     datad4 = datad4(goodref)
     goodref=where(datad5 gt badval)
     datad5 = datad5(goodref)
  
     ; Plot box-whisker by depth
     agoodref=where(smdata gt badval)
     smdataa = smdata(agoodref)
     ymin = min(smdataa)*0.9
     ymax = max(smdataa)*1.1
     xvals = indgen(ndepths)+1
     plot = plot([0,ndepths+1],/nodata, $
              margin=[0.1,0.1,0.02,0.1], $
              yrange=[ymin,ymax], ystyle=1, yticklen=0.04, $
              xrange=[0,ndepths+1], xticklen=0.02, $
              xtickvalues=xvals, xtickname=dnames, xminor=0)
     plot.title=titlepd
     plot.title.font_size=12
     plot.title.font_name='Hershey'
     plot.ytitle=slabel
     plot.xtitle=dlabel
  
     boxwhisk_sm, datad1, xlocation=1
     boxwhisk_sm, datad2, xlocation=2
     boxwhisk_sm, datad3, xlocation=3
     boxwhisk_sm, datad4, xlocation=4
     boxwhisk_sm, datad5, xlocation=5

  endif

  if (pbsite) then begin
     ; Pull out all data by site
     nds = ndepths*ntimes
     datasa = fltarr(nsites,nds)
     count1=0
     count2=nds-1
     for s=0,nsites-1 do begin
         datasa(s,count1:count2) = smdata(s,*,*)
     endfor

     ; Plot
     agoodref=where(smdata gt badval)
     smdataa = smdata(agoodref)
     ymin = min(smdataa)*0.9
     ymax = max(smdataa)*1.1
     xvals = indgen((nsites+1)/2)*2+1
     splot = plot([0,nsites+1],/nodata, $
              margin=[0.1,0.1,0.02,0.1], $
              yrange=[ymin,ymax], ystyle=1, yticklen=0.04, $
              xrange=[0,nsites+1], xticklen=0.02, $
              xtickvalues=xvals, xtickname=xvals, xminor=0)
     splot.title=titleps
     splot.title.font_size=12
     splot.title.font_name='Hershey'
     splot.ytitle=slabel
     splot.xtitle='Site'

     for s=0,nsites-1 do begin
        dhere = datasa(s,*)
        goodref=where(dhere gt badval)
        dhere = dhere(goodref)
        boxwhisk_sm, dhere, xlocation=s+1
     endfor
  endif  ;pbsite

  if (pbtime) then begin
     ; Pull out all data by time
     ndt = nsites*ndepths
     datata = fltarr(ntimes,ndt)
     count1=0
     count2=ndt-1
     for t=0,ntimes-1 do begin
         datata(t,count1:count2) = smdata(*,*,t)
     endfor

     ; Plot
     agoodref=where(smdata gt badval)
     smdataa = smdata(agoodref)
     ymin = min(smdataa)*0.9
     ymax = max(smdataa)*1.1
     xvals = indgen(ntimes)+1
     tplot = plot([0,ntimes+1],/nodata, $
              margin=[0.1,0.1,0.02,0.1], $
              yrange=[ymin,ymax], ystyle=1, yticklen=0.04, $
              xrange=[0,ntimes+1], xticklen=0.02, $
              xtickvalues=xvals, xtickname=timenames, xminor=0)
     tplot.title=titlept
     tplot.title.font_size=12
     tplot.title.font_name='Hershey'
     tplot.ytitle=slabel
     tplot.xtitle='Date'

     for t=0,ntimes-1 do begin
        dhere = datata(t,*)
        goodref=where(dhere gt badval)
        dhere = dhere(goodref)
        boxwhisk_sm, dhere, xlocation=t+1
     endfor
  endif  ;pbsite
  
end
